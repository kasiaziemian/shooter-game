import socket
from _thread import *
from team import Team, Deets
import pickle
import time
from game import Game

class Sever:
    def __init__(self, host="", port=5555):
        self.host = host
        self.port = port
        self.games = {}
        self.id_count = 0

        game_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            game_socket.bind((host, port))
        except socket.error as e:
            str(e)

        game_socket.listen(2)
        print("Waiting for a connection, Server Started")


    def threaded_client(conn, team_id, game_id, id_count):
        conn.send(str.encode(str(team_id)))
    
        while True:
            data = pickle.loads(conn.recv(2048))
            if game_id in games: 
                game = games[game_id]

            if not data:
                print("Disconnected")
                break
            else:
                game.put_deets(data)
                conn.sendall(pickle.dumps(game.get_deets()))
        print("Lost connection")
        try:
            del games[game_id]
            print("Closing Game", game_id)
        except:
            pass
        id_count -= 1
        conn.close()

    
        while game_id == 0:
            conn, address = game_socket.accept()
            print("Connected to:", address)

            id_count += 1
            teamID = 0
            mode = int(conn.recv(2048).decode())
        players = mode if mode != 3 else 2

        if id_count % players == 1:
            game_id += 1
            games[gameID] = Game(game_id, mode)
            print("Creating a new mode", mode, "game, id = ", game_id)
        else:
            teamID = len(games[game_id].get_deets())

        if id_count % players == 0:
            games[game_id].ready = True

        with open("games.dat", "ab") as file:
            pickle.dump([time.time(), address, mode, game_id], file)
            file.close()

        start_new_thread(threaded_client, (conn, team_id, game_id))


