from team import *


class Game:
    def __init__(self, game_id, mode):
        self.ready = False
        self.game_id = game_id
        self.mode = mode
        self.teams = {0: Team(0)}

    def get_deets(self):
        deets = {}
        for team in self.teams:
            deets[self.teams[team].teamID] = self.teams[team].deets
        return deets

    def put_deets(self, deets):
        try:
            self.teams[deets.team_id].deets = deets
        except:
            self.teams[deets.team_id] = Team(deets.team_id)
            self.teams[deets.team_id].deets = deets
        if deets.hit:
            self.reset_current()
        self.teams[deets.team_id].current = True

    def connected(self):
        return self.ready

    def winner(self):
        for team in self.teams:
            if self.teams[team].lives > 0:
                return team
        return None

    def all_current(self):
        for team in self.teams:
            if not self.teams[team].current:
                return False
        return True

    def reset_current(self):
        for team in self.teams:
            self.teams[team].deets.current = False
