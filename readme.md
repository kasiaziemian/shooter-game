# Shooter game
Online space shooter game using client-server communication model. Includes single and multiplayer mode with varying difficulty levels. 


# Run 
```
$ cd shooter-game
$ .\venv\Scripts\activate
$ pip install -r requirements.txt
$ python main.py
```
