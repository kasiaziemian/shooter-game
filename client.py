import socket
import pickle


class Network:
    def __init__(self, mode):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = ""
        self.port = 5555
        self.address = (self.host, self.port)
        self.team_id = self.connect(mode)

    def get_team_id(self):
        return self.team_id

    def connect(self, mode):
        try:
            self.client.connect(self.address)
            self.client.send(str.encode(str(mode)))
            return self.client.recv(2048).decode()
        except:
            pass

    def send(self, data):
        try:
            self.client.send(pickle.dumps(data))
            return pickle.loads(self.client.recv(2048))
        except socket.error as e:
            print(e)
