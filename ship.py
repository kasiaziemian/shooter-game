from team import *
from config import *

class Ship:
    def __init__(self, team, x=gridleft + 10 * size, y=gridtop + 14 * size, direction=0, rank=0, target=None):
        self.direction = direction
        self.speed = 0
        self.team = team
        self.shot = False
        self.shot_speed = 5
        self.hit = False
        self.inert = False 
        self.rect = pygame.Rect(x, y, size, size)
        self.shot_rect = pygame.Rect(x, y, size, size)
        self.shot_direction = 0
        self.autopilot = rank != 0

        self.can_shoot = False
        self.can_take_hit = False

        self.fuel = 10000
        self.lives = 3
        self.delay = 0

        self.target = target
        self.can_see = (5 * random.random()) < rank - 1  
        self.can_shoot = True if self.team.teamID > 1 else (5 * random.random()) < rank - 2
        self.can_follow = (5 * random.random()) < rank - 3
        self.can_sense = (7 * random.random()) < rank - 3
        self.is_fast = (7 * random.random()) < rank - 7
        self.can_take_hit = (5 * random.random()) < rank - 4

        if self.autopilot:
            self.rank = 3 if self.can_follow else 2 if self.can_see else 1
            self.speed = 3 if self.is_fast else 2
        else:
            self.rank = 0

    def update(self):
        self.team.deets.left = self.rect.left
        self.team.deets.top = self.rect.top
        self.team.deets.direction = self.direction
        self.team.deets.speed = self.speed
        self.team.deets.shot = self.shot
        self.team.deets.shotLeft = self.shot_rect.left
        self.team.deets.shotTop = self.shot_rect.top
        self.team.deets.shot_direction = self.shot_direction
        self.team.deets.hit = self.hit

    def reset(self, x, y, direction=-1):
        if direction != -1:
            self.direction = direction
        self.speed = 0
        self.shot = False
        self.hit = False
        self.inert = False
        self.rect.left = x
        self.rect.top = y

    def orient(self, orientation, shot=False):
        if orientation == self.team.orientation:
            return self.shot_rect if shot else self.rect
        else:
            cw90x = (self.team.orientation - orientation) % 4
            return self.rotate(cw90x, shot)

    def rotate(self, cw90x, shot=False):
        x = (self.shot_rect.left if shot else self.rect.left) - grid_left - 15 * size // 2
        y = -((self.shot_rect.top if shot else self.rect.top) - grid_top - 15 * size // 2)

        for i in range(cw90x):
            x, y = y, -x

        left = x + grid_left + 15 * size // 2 - (size if (cw90x % 4) in (1, 2) else 0)
        top = -y + grid_top + 15 * size // 2 - (size if (cw90x % 4) in (2, 3) else 0)
        return pygame.Rect(left, top, size, size)

    def revive(self, factor=3): 
        while True:
            left = int((8 * random.random()) // 1 * size * 2 + grid_left)
            top = int((8 * random.random()) // 1 * size * 2 + grid_top)
            rect = pygame.Rect(left, top, size, size)

            for ship in self.team.enemy:
                shipRect = ship.orient(self.team.orientation)
                if shipRect.left == left or shipRect.top == top or hit(shipRect, rect, factor):
                    break
            else:
                self.rect.left = left
                self.rect.top = top
                self.inert = False
                break

    def retarget(self):
        rect1 = self.target.orient(self.team.orientation)
        rect2 = self.rect
        dist2 = pow(rect1.top - rect2.top, 2) + pow(rect1.left - rect2.left, 2)
        for ship in self.team.enemy:
            rect1 = ship.orient(self.team.orientation)
            if pow(rect1.top - rect2.top, 2) + pow(rect1.left - rect2.left, 2) < dist2 and not ship.inert:
                self.target = ship
                dist2 = pow(rect1.top - rect2.top, 2) + pow(rect1.left - rect2.left, 2)

    def move(self, key=0):
        if key < 0:
            self.retarget()
            self.speed = key * -1
            self.turn()
            if not clear(self, self.team.ships):
                self.direction = (self.direction + 2) % 4
            if self.can_shoot:
                self.autoshoot()

        else:
            if ((self.rect.top - grid_top) / 2) % size == 0: 
                if key in [K_LEFT, K_4, K_a]:
                    self.direction = 3
                elif key in [K_RIGHT, K_6, K_d]:
                    self.direction = 1
            if ((self.rect.left - grid_left) / 2) % size == 0: 
                if key in [K_UP, K_8, K_w]:
                    self.direction = 0
                elif key in [K_DOWN, K_2, K_s]:
                    self.direction = 2
            if key in [K_RCTRL, K_LCTRL, K_5]:
                self.speed = 0
            if key in [K_LEFT, K_4, K_a, K_RIGHT, K_6, K_d, K_UP, K_8, K_w, K_DOWN, K_2, K_s]:
                self.speed = 3

            if key in [K_SPACE] or self.shot:
                self.shoot()

        if self.direction == 0:
            self.rect.top = max(self.rect.top - self.speed, grid_top)
        elif self.direction == 1:
            self.rect.left = min(self.rect.left + self.speed, grid_left + 14 * size)
        elif self.direction == 2:
            self.rect.top = min(self.rect.top + self.speed, grid_top + 14 * size)
        else: 
            self.rect.left = max(self.rect.left - self.speed, grid_left)

        self.fuel -= max(self.speed, 1)
        self.update()

    def shoot(self):
        if not self.shot:
            self.shot = True
            self.shot_rect = self.rect.copy()
            self.shot_direction = self.direction

            if self.direction == 3:
                self.shot_rect.top = self.rect.top
                self.shot_rect.left = self.rect.left - size // 2

            elif self.direction == 1:
                self.shot_rect.top = self.rect.top
                self.shot_rect.left = self.rect.left + size // 2

            elif self.direction == 0:
                self.shot_rect.top = self.rect.top - size // 2
                self.shot_rect.left = self.rect.left

            elif self.direction == 2:
                self.shot_rect.top = self.rect.top + size // 2
                self.shot_rect.left = self.rect.left

        else:
            if self.shot_direction == 0:
                self.shot_rect.top = self.shot_rect.top - self.shot_speed

            elif self.shot_direction == 1:
                self.shot_rect.left = self.shot_rect.left + self.shot_speed

            elif self.shot_direction == 2:
                self.shot_rect.top = self.shot_rect.top + self.shot_speed
                
            else:
                self.shot_rect.left = self.shot_rect.left - self.shot_speed

    def turn(self):
        r = random.random()
        targetRect = self.target.orient(self.team.orientation)
        if ((self.rect.top - grid_top) / 2) % size == 0 and ((self.rect.left - grid_left) / 2) % size == 0:
            if self.can_sense and not self.target.inert and r < .1:
                if self.rect.top < targetRect.top:
                    self.direction = 2

                if self.rect.top > targetRect.top:
                    self.direction = 0

            elif self.can_sense and not self.target.inert and r < .2: 
                if self.rect.left < targetRect.left:
                    self.direction = 1

                if self.rect.top > targetRect.top:
                    self.direction = 3

            if self.can_follow and not self.target.inert: 
                if self.rect.top == targetRect.top + 2 * size:
                    self.direction = 0

                elif self.rect.top == targetRect.top - 2 * size:
                    self.direction = 2

                elif self.rect.left == targetRect.left + 2 * size:
                    self.direction = 3

                elif self.rect.left == targetRect.left - 2 * size:
                    self.direction = 1

            if self.can_see and not self.target.inert:  
                if self.rect.top == targetRect.top and self.rect.left > targetRect.left:
                    self.direction = 3

                elif self.rect.top == targetRect.top and self.rect.left < targetRect.left:
                    self.direction = 1

                elif self.rect.top > targetRect.top and self.rect.left == targetRect.left:
                    self.direction = 0

                elif self.rect.top < targetRect.top and self.rect.left == targetRect.left:
                    self.direction = 2
            if r < .15:  
                self.direction = (self.direction - 1) % 4

            elif r < .3: 
                self.direction = (self.direction + 1) % 4

            if self.direction == 0 and self.rect.top == grid_top or \
                    self.direction == 1 and self.rect.left == grid_left + 14 * size or \
                    self.direction == 2 and self.rect.top == grid_top + 14 * size or \
                    self.direction == 3 and self.rect.left == grid_left: 
                self.direction = (self.direction + 1 if r < .5 else 3) % 4

            if not clear(self, self.team.ships):
                self.direction = (self.direction + 2) % 4

    def autoshoot(self):
        if not self.shot and not self.inert: 
            targetRect = self.target.orient(self.team.orientation)

            if not self.target.hit: 
                if self.rect.top == targetRect.top and self.rect.left > targetRect.left\
                        and self.direction == 3 and not self.target.inert:
                    self.shot_rect = self.rect.copy()
                    self.shot_rect.left = self.rect.left - size // 2
                    self.shot_direction = self.direction
                    self.shot = True

                elif self.rect.top == targetRect.top and self.rect.left < targetRect.left\
                        and self.direction == 1 and not self.target.inert:
                    self.shot_rect = self.rect.copy()
                    self.shot_rect.left = self.rect.left + size // 2
                    self.shot_direction = self.direction
                    self.shot = True

                elif self.rect.top > targetRect.top and self.rect.left == targetRect.left\
                        and self.direction == 0 and not self.target.inert:
                    self.shot_rect = self.rect.copy()
                    self.shot_rect.top = self.rect.top - size // 2
                    self.shot_direction = self.direction
                    self.shot = True

                elif self.rect.top < targetRect.top and self.rect.left == targetRect.left\
                        and self.direction == 2 and not self.target.inert:
                    self.shot_rect = self.rect.copy()
                    self.shot_rect.top = self.rect.top + size // 2
                    self.shot_direction = self.direction
                    self.shot = True

        else: 
            if self.shot_direction == 0:
                self.shot_rect.top = self.shot_rect.top - self.shot_speed

            elif self.shot_direction == 1:
                self.shot_rect.left = self.shot_ect.left + self.shot_speed

            elif self.shot_direction == 2:
                self.shot_rect.top = self.shot_rect.top + self.shot_speed
                
            else:  
                self.shot_rect.left = self.shot_tect.left - self.shot_speed
            if self.shot_rect.top - grid_top < -size // 2 or grid_top + 15 * size - self.shot_rect.top < size // 2 or \
                    self.shot_rect.left - grid_left < -size // 2 or grid_left + 15 * size - self.shot_rect.left < size // 2:
                self.shot = False


def hit(rect1, rect2, factor):
    return pow(pow(rect1.top - rect2.top, 2) + pow(rect1.left - rect2.left, 2), .5) < size * factor


def clear(ship, ships):
    rect = ship.rect.copy()
    if ship.direction == 0:
        rect.top -= ship.speed
    if ship.direction == 1:
        rect.left += ship.speed
    if ship.direction == 2:
        rect.top += ship.speed
    if ship.direction == 3:
        rect.left -= ship.speed
    for other in ships:
        if hit(other.rect, rect, .5) and ship is not other:
            return False
    return True
