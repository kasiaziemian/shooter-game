import pygame
from pygame.locals import *
import random

from config import *
from ship import *


class Deets:
    def __init__(self, teamID, lives, left, top, direction, speed, shot, shotLeft, shotTop, shotDirection, orientation, hit, current):
        self.team_id = team_id
        self.lives = lives
        self.left = left
        self.top = top
        self.direction = direction
        self.speed = speed
        self.shot = shot
        self.shotLeft = shotLeft
        self.shotTop = shotTop
        self.shotDirection = shotDirection
        self.orientation = orientation
        self.hit = hit
        self.current = current

class Team:
    def __init__(self, team_id, orientation=0, x=0, y=0, direction=0, rank=0):
        self.team_id = team_id
        self.score: int = 0
        self.ships = [Ship(self, rank=rank)] if x == 0 else [Ship(self, x, y, direction, rank)]
        self.enemy = []
        self.target = None
        self.orientation = orientation
        self.deets = Deets(team_id, 3, self.ships[0].rect.left, self.ships[0].rect.top,
                           self.ships[0].direction, self.ships[0].speed,
                           self.ships[0].shot, self.ships[0].shotRect.left, self.ships[0].shotRect.top,
                           self.ships[0].shotDirection, self.orientation, self.ships[0].hit, True)
        self.lives: int = 3
        self.level = 0
        self.current = True

    @property
    def current(self):
        return self._current

    @current.setter
    def current(self, value):
        self.deets.current = value
        self._current = value

    @property
    def lives(self):
        return self._lives

    @lives.setter
    def lives(self, value):
        self.deets.lives = value
        self._lives = value

    def update(self, deets):  
        self.deets = deets
        self.lives = self.deets.lives
        self.ships[0].rect.left = deets.left
        self.ships[0].rect.top = deets.top
        self.ships[0].direction = deets.direction
        self.ships[0].speed = deets.speed
        self.ships[0].shot = deets.shot
        self.ships[0].shotRect.left = deets.shotLeft
        self.ships[0].shotRect.top = deets.shotTop
        self.ships[0].shotDirection = deets.shotDirection
        self.ships[0].hit = deets.hit
        self.current = deets.current
        return deets


