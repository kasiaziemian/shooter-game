from shooter_game import *

class Button:
    def __init__(self, text, x, y, width, height, team_id = -1):
        self.text = text
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.team_id = team_id
        self.info = pygame.Rect(500, 90, 450, 450)

    def draw(self, screen, info):
        if self.team_id < 0:
            pygame.draw.rect(screen, (0, 128, 0), (info.left + self.x, info.top + self.y, self.width, self.height))
            text = font20.render(self.text, 1,  (0, 0, 0))
            screen.blit(text, (info.left + self.x + self.width//2 - text.get_width()//2,
                               info.top + self.y + self.height//2 - text.get_height()//2))
        else:
            pygame.draw.rect(screen, (0, 128, 0), (info.left + self.x, info.top + self.y, self.width, self.height))
            pygame.draw.rect(screen, (0, 0, 0), (info.left + self.x + 5, info.top + self.y + 5, self.width - 10, size + 5))
            screen.blit(pygame.image.load("images/ship" + self.text[0] + "00.gif"),
                        (info.left + self.x + 25, info.top + self.y + 10, size, size))
            text = font20.render(self.text, 1,  (0, 0, 0))
            screen.blit(text, (info.left + self.x + (size + 50)//2 - text.get_width()//2,
                               info.top + self.y + self.height - text.get_height()))

    def click(self, pos):
        x1 = pos[0] - info.left
        y1 = pos[1] - info.top
        if self.x <= x1 <= self.x + self.width and self.y <= y1 <= self.y + self.height:
            return True
        else:
            return False