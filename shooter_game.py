import pickle
import pygame
import random
import sys
import time

from team import Team, Ship, hit, Deets
from client import *
from config import *
from pygame.locals import *
from button import *

pygame.init()
pygame.mixer.init()

class Main:
    def __init__(self):
        self.start = start 
        self.show_lives = []
        self.team = []
        self.fuel = 0
        self.scores = []
        self.x = x 
        self.y = y 
        self.ship_images = []
        self.shot_images = []
        self.delay = 0
        self.players = 0
        self.teams = []
        self.booms = []
        self.screen = pygame.display.set_mode(screensize)
        self.info = pygame.Rect(500, 90, 450, 450)

    def start(self):
        for team in teams:
            for ship in team.ships:
                ship.speed = 0
                ship.shot = False

        draw_screen()

        screen.blit(pygame.Surface((300, 100)), (info.left + 30, info.top + 190))
        screen.blit(font40.render("Ready", True, (180, 0, 0)), (info.left + 160, info.top + 190))
        pygame.display.update((info.left + 30, info.top + 140, 400, 150))

        now = time.time()
        while time.time() - now < 1:
            pass

        screen.blit(pygame.Surface((300, 100)), (info.left + 80, info.top + 190))
        screen.blit(font40.render("Set", True, (180, 0, 0)), (info.left + 180, info.top + 190))
        pygame.display.update((info.left + 80, info.top + 160, 300, 150))

        now = time.time()
        while time.time() - now < 1:
            pass

        screen.blit(pygame.Surface((300, 100)), (info.left + 80, info.top + 190))
        screen.blit(font40.render("Go!", True, (180, 0, 0)), (info.left + 180, info.top + 190))
        pygame.display.update((info.left + 80, info.top + 160, 300, 150))


    def load_images(self):
        for team in range(4):
            lvl_images = []
            for lvl in range(4):
                spd_images = []
                for spd in range(4):
                    spd_images.append(pygame.image.load("images/ship" + str(image_map[team]) + str(lvl) + str(spd) + ".gif"))
                lvl_images.append(spd_images)
            ship_images.append(lvl_images)
            shot_images.append(pygame.image.load("images/shot" + str(team) + ".gif"))

        if image_map[-1] == -1:
            ship_images[teams[0].team_id][0][0] = pygame.image.load("images/ship" + str(teams[0].team_id) + "00.gif")
            ship_images[teams[0].team_id][0][3] = pygame.image.load("images/ship" + str(teams[0].team_id) + "03.gif")
            shot_images[teams[0].team_id] = pygame.image.load("images/shot" + str(teams[0].team_id) + ".gif")

        else:
            my_id = teams[0].team_id
            ship_images[my_id][0][0] = pygame.image.load("images/ship" + str(image_map[image_map[-1]]) + "00.gif")
            ship_images[my_id][0][3] = pygame.image.load("images/ship" + str(image_map[image_map[-1]]) + "03.gif")
            shot_images[my_id] = pygame.image.load("images/shot" + str(image_map[image_map[-1]]) + ".gif")
            ship_images[image_map[image_map[-1]]][0][0] = pygame.image.load("images/ship" + str(my_id) + "00.gif")
            ship_images[image_map[image_map[-1]]][0][3] = pygame.image.load("images/ship" + str(my_id) + "03.gif")
            shot_images[image_map[image_map[-1]]] = pygame.image.load("images/shot" + str(my_id) + ".gif")


    def show_lives(self, info):
        if players == 1:
            lives = teams[0].lives
        lifeleft = info.left + 116
        lifetop = info.top + 150
        for life in range(lives):
            screen.blit(ship_images[int(0 if image_map[-1] == -1 else image_map[-1])][0][0], (lifeleft, lifetop, size, size))
            lifeleft = lifeleft + size * 1.2
            if life in (2, 4):
                lifeleft = info.left + 116
                lifetop = lifetop + size
        else:
            for team in range(4 if players > 2 else 2):
                if teams[team].lives == 0:
                    screen.blit(font20.render("Spectating", True, (180, 0, 0)),
                            (info.left + 40 + 260 * (teams[team].teamID % 2),
                             info.top + (110 if team > 1 else 50)))
                for life in range(teams[team].lives):
                    show_life(teams[team], life)

    def show_life(self, team, life):
        lifeleft = info.left + 30 + 50 * life + (info.width - 90 - 100 * life) * (team.teamID % 2)
        lifetop = info.top + (110 if team.teamID > 1 else 50)
        screen.blit(ship_images[team.teamID][0][0], (lifeleft, lifetop, size, size))
        return pygame.Rect(lifeleft, lifetop, size, size)


    def draw_fuel_bar(self, fuel, x=info.left + 32, y=info.top + 149):
        if fuel < 0:
            fuel = 0

    bar_length = 23
    bar_height = 158
    fill = bar_height * fuel // 10000
    fill_rect = pygame.Rect(x, y + bar_height - fill,  bar_length, fill)
    colour = [0, 204, 0] if fuel >= 5000 else [255, 128, 0] if fuel >= 2500 else [255, 0, 0]
    pygame.draw.rect(screen, [0, 128, 0], pygame.Rect(x - 5, y - 5, bar_length + 10, bar_height + 10))
    pygame.draw.rect(screen, [0, 0, 0], pygame.Rect(x, y, bar_length, bar_height))
    pygame.draw.rect(screen, colour, fill_rect)


    def game_over(self, team):
        if team is None:
            return

        screen.blit(pygame.Surface((200, 100)), (info.left + 280, info.top))
        screen.blit(pygame.Surface((500, 500)), (info.left, info.top + 40))
        screen.blit(font40.render("Game Over", True, (180, 0, 0)), (info.left + 120, info.top + 90))
        pygame.display.flip()

        if players == 1:
            try:
                with open("singles.dat", "rb") as file:
                    scores = pickle.load(file)
                    file.close()
            except FileNotFoundError:
                scores = [["Tinky-Winky", 100, 5], ["Dipsy", 80, 4], ["La-La", 60, 3], ["Po", 40, 2], ["Noo-Noo", 20, 1]]

        player_name = ""
        for i in range(5):
            if team.score > scores[i][1]:
                screen.blit(font20.render("You got a High Score!", True, (180, 0, 0)), (info.left + 100, info.top + 190))
                screen.blit(font20.render("Please enter your name:", True, (180, 0, 0)), (info.left + 100, info.top + 240))
                while True:
                    pygame.display.update((info.left, info.top, info.width, info.height))
                    while True:
                        event = pygame.event.poll()
                        if event.type == KEYDOWN:
                            inkey = event.key
                            break
                    if inkey == K_BACKSPACE:
                        player_name = player_name[:len(player_name) - 1]
                    elif inkey == K_RETURN or inkey == K_KP_ENTER:
                        break
                    elif inkey <= 127:
                        player_name = player_name + chr(inkey - (32 if len(player_name) == 0 else 0))

                    screen.blit(pygame.Surface((200, 50)), (info.left + 80, info.top + 320))
                    screen.blit(font20.render(player_name, True, (180, 0, 0)), (info.left + 80, info.top + 320))

                scores.insert(i, [str(player_name), team.score, team.level])
                scores.pop()
                break

        screen.blit(pygame.Surface((150, 100)), (info.left + 160, info.top))
        screen.blit(pygame.Surface((450, 445)), (info.left, info.top))  # top + 40
        screen.blit(font40.render("High Scores", True, (180, 0, 0)), (info.left + 100, info.top + 70))
        screen.blit(font20.render("Player      Level    Score", True, (180, 0, 0)), (info.left + 80, info.top + 130))
        screen.blit(font20.render("Press heRe to restart", True, (180, 0, 0)), (info.left + 100, info.top + 400))

        for i in range(5):
            colour = (0, 128, 0) if scores[i][1] == team.score and scores[i][0] == player_name else (180, 0, 0)
            screen.blit(font20.render('{:<15}'.format(scores[i][0]), True, colour),
                        (info.left + 80, info.top + 160 + i * 25))
            screen.blit(font20.render('{:>7}'.format(str(scores[i][2])), True, colour),
                        (info.left + 165, info.top + 160 + i * 25))
            screen.blit(font20.render('{:>14}'.format(str(scores[i][1])), True, colour),
                        (info.left + 210, info.top + 160 + i * 25))

        with open("singles.dat", "wb") as file:
            pickle.dump(scores, file)
            file.close()
        else:
            screen.blit(font20.render("The winner is ", True, (180, 0, 0)), (info.left + 115, info.top + 190))
            screen.blit(ship_images[team.teamID][0][0], (info.left + 280, info.top + 185, size, size))
            if teams[0].teamID == team.teamID:
                screen.blit(font20.render("Congratulations!", True, (180, 0, 0)), (info.left + 120, info.top + 240))
                s_boom[0].play()

            else:
                screen.blit(font20.render("Better luck next time", True, (180, 0, 0)), (info.left + 100, info.top + 240))

            with open("multi" + str(players) + ".dat", "wb") as file:
                pickle.dump([time.time(), players, team], file)
                file.close()

        screen.blit(font20.render("Press any key", True, (180, 0, 0)), (info.left + 130, info.top + 320))
        screen.blit(font20.render("to return to the main menu", True, (180, 0, 0)), (info.left + 70, info.top + 350))
        pygame.display.flip()


    def round_over(self, victor, screen, teams, info):
        screen.blit(pygame.Surface((490, 450)), (info.left, info.top))
        result = "Victory!" if victor is teams[0] else "Defeat!"
        screen.blit(font40.render(result, True, (180, 0, 0)), (info.left + 160, info.top + 90))

        victor.score += 1
        screen.blit(font20.render("Your score", True, (180, 0, 0)), (info.left + 80, info.top + 190))
        screen.blit(font20.render("Their score", True, (180, 0, 0)), (info.left + 280, info.top + 190))
        screen.blit(font20.render('{:<15}'.format(teams[0].score), True, (180, 0, 0)), (info.left + 130, info.top + 240))
        screen.blit(font20.render('{:<15}'.format(teams[1].score), True, (180, 0, 0)), (info.left + 330, info.top + 240))

        screen.blit(font20.render("Press R to Ready-up", True, (180, 0, 0)), (info.left + 100, info.top + 340))
        pygame.display.flip()


    def crash(self, ship, show_life, booms, players, teams):
        hitter = -1
        shipRect = ship.orient(teams[0].orientation)
        s_side = pygame.mixer.Sound("sounds/side.wav")

        for target in ship.team.enemy:
            targetRect = target.orient(teams[0].orientation)

            if hit(targetRect, shipRect, .7) and not ship.inert and not target.inert:
                booms.append(shipRect.copy())
                booms.append(targetRect.copy())
                if players > 1:
                    booms.append(show_life(ship.team, ship.team.lives - 1))
                    booms.append(show_life(target.team, target.team.lives - 1))    
                s_boom[0].play()
                ship.hit = True
                target.hit = True
                hitter = target.team.teamID

            if ship.shot:
                shipShotRect = ship.orient(teams[0].orientation, True)
                if hit(targetRect, shipShotRect, .7) and not target.inert:
                    target.hit = True
                    ship.shot = False
                    booms.append(targetRect.copy())
                    if players > 1:
                        booms.append(show_life(target.team, target.team.lives - 1))
                    if target.can_take_hit:
                        s_bzzt = pygame.mixer.Sound("sounds/bzzt.wav")
                        s_bzzt.play()
                    else:
                        s_boom[0 if players > 1 else len(ship.team.enemy)-1].play()
                    hitter = ship.team.teamID
                if ship.shotRect.top + size // 2 < grid_top or ship.shotRect.top + size // 2 > grid_top + 15 * size or \
                        ship.shotRect.left + size // 2 < grid_left or ship.shotRect.left + size // 2 > grid_left + size * 15:
                    ship.shot = False
                    s_side.play()

            if target.shot:
                targetShotRect = target.orient(teams[0].orientation, True)
                if hit(targetShotRect, shipRect, .7) and not ship.inert:
                    ship.hit = True
                    target.shot = False
                    booms.append(shipRect.copy())
                    if players > 1:
                        booms.append(show_life(ship.team, ship.team.lives - 1))
                    s_boom[0].play()
                    hitter = target.team.teamID
                if target.shotRect.top + size // 2 < grid_top or target.shotRect.left + size // 2 < grid_left or \
                        target.shotRect.top + size // 2 > grid_top + 15 * size or \
                        target.shotRect.left + size // 2 > grid_left + size * 15:
                    print("enemy shot hit a wall")
                    s_side.play()
                    target.shot = False

        return hitter


    def show_booms(self, screen, booms):
        for boom in booms:
            boomimg = pygame.image.load("images/boom.gif")
            screen.blit(pygame.transform.rotate(pygame.transform.scale(boomimg, (boom.width, boom.height)),
                                            boom.width * -90), boom)
            boom.height -= min(boom.height, (1 + size - boom.height))
            boom.top += (boom.width - boom.height) // 2
            boom.left += (boom.width - boom.height) // 2
            boom.width = boom.height
            if boom.width <= 0:
                booms.remove(boom)


    def draw_screen(self, info, show_lives, draw_fuel_bar, screen, players, teams, shot_images, ship_images):
        grid = pygame.image.load("images/grid.gif")
        grid_rect = grid.get_rect()
        screen.blit(grid, grid_rect)
        screen.blit(pygame.Surface((info.width, info.height)), (info.left, info.top))
        show_booms()

        if players == 1:
            info_1p = pygame.image.load("images/info_1p.gif")
            screen.blit(info_1p, info)
            screen.blit(font40.render('{:<10}'.format("Score"), True, (0, 150, 0)), (info.left, info.top + 5))
            screen.blit(font40.render('{:>10}'.format(str(teams[0].score)), True, (0, 150, 0)),
                    (info.left + 60, info.top + 5))
            screen.blit(font40.render('{:<10}'.format("Level"), True, (0, 150, 0)), (info.left, info.top + 45))
            screen.blit(font40.render('{:>10}'.format(str(teams[0].level)), True, (0, 150, 0)),
                    (info.left + 60, info.top + 45))

            screen.blit(font40.render('{:<10}'.format("Fuel"), True, (0, 150, 0)), (info.left, info.top + 100))
            draw_fuel_bar(teams[0].ships[0].fuel)
            screen.blit(font40.render('{:<10}'.format("Ships"), True, (0, 150, 0)), (info.left + 116, info.top + 100))
        elif players > 1:
            screen.blit(font40.render("Go!", True, (180, 0, 0)), (info.left + 180, info.top + 190))

        if len(teams) > 0:
            show_lives()

        for team in teams:
            for ship in team.ships:
                rotation = (ship.direction + team.orientation + teams[0].orientation) * -90
                if not ship.inert:
                    image = ship_images[image_map[team.teamID]][ship.rank][ship.speed]
                    screen.blit(pygame.transform.rotate(image, rotation), ship.orient(teams[0].orientation))
                    if ship.can_take_hit:
                        shield = pygame.image.load("images/shield.gif")
                        screen.blit(shield, ship.orient(teams[0].orientation))
                    if ship.canShoot and not ship.shot:
                        image = shot_images[image_map[team.teamID]]
                        screen.blit(pygame.transform.rotate(image, rotation), ship.orient(teams[0].orientation))
                if ship.shot:
                    image = shot_images[image_map[team.teamID]]
                    rotation = (ship.shotDirection + team.orientation + teams[0].orientation) * -90
                    screen.blit(pygame.transform.rotate(image, rotation), ship.orient(teams[0].orientation, True))

        pygame.display.flip()


    def no_shots(self, teams):
        for team in teams:
            if team.ships[0].shot:
                return False
        return True


    def update(self, network, players, teams):
        if players == 3:
            network.send(teams[2].deets)
        deets = network.send(teams[0].deets)
        if players == 3:
            teams[1].update(deets[1 - teams[0].teamID])
            teams[3].update(deets[3 - teams[0].teamID])
        elif players == 4:
            for t in range(3):
                try:
                    teams[t+1].update(deets[teams[t+1].teamID])
                except:
                    pass
        return deets

    def pvp(self, load_images, start, teams, screen, players, info):
        network = Network(players)
        clock = pygame.time.Clock()
        key = 0
        my_id = int(network.get_team_id())
        if my_id is None:
            print("No server")
            screen.blit(pygame.Surface((info.width, info.height)), (info.left, info.top))
            screen.blit(font20.render("Server unavailable", True, (180, 0, 0)), (info.left + 110, info.top + 140))
            screen.blit(font20.render("Please select Single Player option", True, (180, 0, 0)), (info.left + 30, info.top + 190))
            screen.blit(font20.render("Press any key", True, (180, 0, 0)), (info.left + 125, info.top + 320))
            screen.blit(font20.render("to return to the main menu", True, (180, 0, 0)), (info.left + 65, info.top + 370))
            pygame.display.update(info)
            return
        teams = [Team(my_id)]  
        pygame.display.set_caption("PyForce - P" + str(my_id))

        screen.blit(pygame.Surface((490, 450)), (info.left, info.top))
        screen.blit(font20.render("Waiting for opponent...", True, (180, 0, 0)), (info.left + 80, info.top + 190))
        pygame.display.update(info)

        while True:
            data = network.send(teams[0].deets)
            users = players if players != 3 else 2
            if len(data) == users:
                break

        if players == 3:
            teams.append(Team(1 - my_id, 2))  
            teams.append(Team(my_id + 2, 0, grid_left + 14 * size, grid_top + 4 * size, 3, 7))
            network.send(teams[2].deets)
            teams[2].ships[0].target = teams[0].ships[0]
            teams.append(Team(3 - my_id, 2, grid_left + 14 * size, grid_top + 4 * size, 3, 7))
            teams[2].ships[0].canTakeAHit = False
            teams[3].ships[0].canTakeAHit = False

        elif players == 4:
            teams.append(Team((my_id + 1) % 4, 3))
            teams.append(Team((my_id + 2) % 4, 2))
            teams.append(Team((my_id + 3) % 4, 1))

        screen.blit(font20.render("Setting up teams....", True, (180, 0, 0)), (info.left + 85, info.top + 290))
        pygame.display.update((info.left + 80, info.top + 240, 400, 200))
        time.sleep(1)

        deets = network.send(teams[0].deets)

        for teamA in teams:
            teamA.deets = deets[teamA.teamID]
            for teamB in teams:
                if teamA.teamID != teamB.teamID:
                    for ship in teamB.ships:
                        teamA.enemy.append(ship)

        load_images()
        draw_screen()
        ship_hit = False
        start()

        while True:
            teams[0].ships[0].move(key)  
            if players >= 3:
                teams[2].ships[0].move(-3 if players == 3 else 0) 

            update(network)

            for team in teams:
                crash(team.ships[0])

                if team.ships[0].hit:
                    team.ships[0].update()
                    ship_hit = True
                    team.lives -= 1
                    team.ships[0].inert = True

                    if team.teamID in {my_id, my_id + 2}:
                        update(network)
                    team.ships[0].hit = False
                    team.ships[0].update()

            if ship_hit:
                while True:
                    deets = update(network)
                    for deet in deets:
                        if not deets[deet].current:
                            break
                    else:
                        ship_hit = False
                        break

            teams_left = 0
            winner = None

            for team in teams:
                if team.lives > 0:
                    teams_left += 1
                    winner = team  

                    if team.ships[0].inert and not team.ships[0].shot:
                        if team.teamID in (my_id, my_id + 2): 
                            team.ships[0].revive(5)
                        else:
                            team.ships[0].inert = False

            if teams_left <= 1:
                update(network)
                delay = 10  
                while delay:
                    time.sleep(1.0 / 50)
                    draw_screen()
                    delay -= 1
                return winner

            update(network)
            draw_screen()

            if key == K_SPACE:
                key = 0 

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()  
                if event.type == pygame.KEYDOWN:
                    key = event.key

            draw_screen()
            clock.tick(50)



    def pve(self, load_images, teams, booms):
        random.seed()
        teams = [Team(0), Team(1, rank=1)]
        ship0 = teams[0].ships[0]
        teams[1].ships = []
        teams[1].target = ship0
        load_images()

        levelable = True
        key = 0
        play = True

        while True:
            while not play:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        sys.exit()
                    if event.type == pygame.KEYDOWN:
                        play = True
                        key = event.key

            if len(teams[1].ships) == 0 and len(booms) == 0:

                ship0.reset(grid_left + 14 * size, grid_top + 14 * size, 0)
                teams[0].score = teams[0].score + 100 * teams[0].level + ship0.fuel // 10 * teams[0].level
                teams[0].level += 1
                ship0.fuel = 10000
                play = False
                key = 0

                for i in range(8):
                    teams[1].ships.append(Ship(teams[1], grid_left + i * 2 * size, grid_top, 2, teams[0].level, teams[0].ships[0]))
                teams[0].enemy = teams[1].ships

            draw_screen()

            if not ship0.hit:
                ship0.move(key)
                ship0.fuel -= ship0.speed
            else:
                ship0.team.lives -= 0 if ship0.inert else 1
                ship0.inert = True
                ship0.move(0) 

                for drone in teams[0].enemy:
                    if drone.shot or ship0.shot:
                        break
                else:
                    if len(booms) == 0:
                        if ship0.team.lives >= 0:
                            if len(teams[1].ships) != 0:
                                ship0.hit = False
                                ship0.inert = False
                                ship0.fuel = (ship0.fuel + 10000) // 2
                                ship0.speed = 0
                                ship0.revive(3)
                                draw_screen()
                                play = False
                        else:
                            draw_screen()
                            return teams[0]

            if key == K_SPACE:
                key = 0

            for drone in teams[1].ships:
                drone.move(-drone.speed)
                if drone.hit:
                    drone.hit = False
                    if drone.canTakeAHit:
                        drone.canTakeAHit = False
                    else:
                        drone.inert = True
                        teams[0].score += 100  
                if drone.inert and not drone.shot:
                    teams[1].ships.remove(drone)
            crash(ship0)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    key = event.key

            if key == K_l and ship0.lives > 0 and levelable:
                teams[0].level += 1
                if teams[0].level == 20:
                    levelable = False
                teams[1].ships = []
                for i in range(8):
                    teams[1].ships.append(Ship(teams[1], grid_left + i * 2 * size, grid_top, 2, teams[0].level, teams[0].ships[0]))
                teams[0].enemy = teams[1].ships
                ship0.reset(grid_left + 14 * size, grid_top + 14 * size, 0)
                ship0.fuel = 10000
                play = False
                draw_screen()

            if key == K_r:
                teams[0].level = 0
                teams[1].ships = []
                teams[0].score = 0
                ship0.reset(grid_left + 14 * size, grid_top + 14 * size, 0)
                ship0.team.lives = 3
                levelable = True
            time.sleep(1.0 / 50)


    def select_team(self, btn, screen, info):
        screen.blit(pygame.Surface((500, 40)), (info.left, info.top + 430))
        text = font40.render("*", 1, (0, 128, 0))
        screen.blit(text, (info.left + btn.x + btn.width // 2 - 13, info.top + btn.y + btn.height + 10))
        pygame.display.update((info.left, info.top + 430, 500, 40))
        image_map[-1] = btn.text[0]


    def main(self, game_over, screen, players, info):
        while True:
            buttons = [Button("Single Player", 125, 60, 180, 50),
                   Button("Head to Head", 125, 120, 180, 50),
                   Button("Battle Royale", 75, 180, 280, 50),
                   Button("Battle Royale", 75, 240, 280, 50),
                   Button("Tinky-Winky", 25, 350, size + 50, size + 40, 0),
                   Button("Dipsy", 125, 350, size + 50, size + 40, 1),
                   Button("La-La", 325, 350, size + 50, size + 40, 2),
                   Button("Po", 225, 350, size + 50, size + 40, 3)]

            draw_screen()

            screen.blit(pygame.Surface((info.width, info.height)), (info.left, info.top))
            text = font40.render("Select Game Mode:", True, (0, 128, 0))
            screen.blit(text, (info.width / 2 - text.get_width() / 2, info.top + 10))
            text = font40.render("Select Team:", True, (0, 128, 0))
            screen.blit(text, (info.width / 2 - text.get_width() / 2, info.top + 300))
            for button in buttons:
                button.draw()

            pygame.display.update(info)

            if image_map[-1] != -1:
                text = font40.render("*", 1, (0, 128, 0))
                btn = buttons[int(image_map[image_map[-1]] + 4)]
                screen.blit(text, (info.left + btn.x + btn.width // 2 - 13, info.top + btn.y + btn.height + 10))
                pygame.display.update((info.left, info.top + 430, 500, 40))

            waiting_for_user = True
            while waiting_for_user:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()

                    if event.type == pygame.MOUSEBUTTONDOWN:
                        click_pos = pygame.mouse.get_pos()
                        for button in buttons:
                            if button.click(click_pos):
                                if button.text == "Tinky-Winky":
                                    select_team(buttons[4])
                                elif button.text == "Dipsy":
                                    select_team(buttons[5])
                                elif button.text == "La-La":
                                    select_team(buttons[7])
                                elif button.text == "Po":
                                    select_team(buttons[6])
                                else:
                                    waiting_for_user = False
                                    user_input = button.text

            if user_input == "Single Player":
                players = 1
                game_over(pve())
            elif user_input == "Head to Head":
                players = 2
                game_over(pvp())
            elif user_input == "Battle Royale":
                players = 3
                game_over(pvp())
            elif user_input == "Battle Royale":
                players = 4
                game_over(pvp())
            else:
                pass
            time.sleep(1)

            players = 0
            t = time.time()
            while True:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                    if event.type == pygame.KEYDOWN:
                        t -= 220
                if time.time() - t > 220:
                    break
